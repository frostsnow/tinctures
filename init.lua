-- This file is part of Tinctures, a Minetest mod for upgrading items
-- Copyright (C) 2021-2023  Wade T. Cline
--
-- Tinctures is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License version 3 as
-- published by the Free Software Foundation.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

-- Increase drops by 2% per tincture by providing a bonus drop when enough
-- ore has been mined.
function tinc_callback_efficiency(pos, oldnode, digger)
	-- Check for efficiency tincture.
	local wielded = digger:get_wielded_item()
	local wielded_meta = wielded:get_meta()
	local efficiency = wielded_meta:get_int("tinctures:efficiency")
	if efficiency == 0 then
		-- No efficiency enchantment(s).
		return
	end

	-- Check for ore.
	-- FIXME: There's got to be a constant-time way to do this.
	local ore = nil
	for _, tmp in pairs(minetest.registered_ores) do
		if tmp.ore == oldnode.name then
			-- Node is an ore.
			ore = tmp
			break
		end
	end
	if ore == nil then
		-- Node not an ore.
		return
	end
	-- Turns out sand, dirt, &c. are registered as "ores" for the purpose
	-- of spawning though they aren't the kind of ore that the mod is
	-- looking for.  This is a hack to only count actual ores and not just
	-- what spawns using ore functions.
	if not string.match(ore.ore, "%a+:stone_with_%a+") then
		-- Not an ore.
		return
	end

	-- Apply efficiency effect.
	local digger_meta = digger:get_meta()
	local extra = digger_meta:get_int("tinctures:efficiency:" .. ore.ore)
	extra = extra + efficiency * 2
	local items = minetest.get_node_drops(oldnode, wielded)
	if extra >= 100 then
		minetest.sound_play("efficiency", {pos = digger:get_pos(),
			gain = 0.2}, true)
	end
	while extra >= 100 do
		-- Add extra item.
		-- I'm not sure how probablistic drops work; specifically,
		-- whether or not 'get_node_drops()' will return what will
		-- actually drop from the node (or if that's even possible).
		-- The assumption is that 'get_node_drops()' returns what
		-- will actually drop.
		extra = extra - 100
		local inventory = digger:get_inventory()
		for _, item in pairs(items) do
			-- Add item.
			if inventory:room_for_item("main", item) then
				-- Add item to inventory.
				inventory:add_item("main", item)
			else
				-- Add item to world.
				minetest.add_item(pos, item)
			end
		end
	end
	digger_meta:set_int("tinctures:efficiency:" .. ore.ore, extra)
	return
end

-- Handles setting and application of tincture metadata both when applying an
-- essence, applying a tincture, and when repairing tools.  This gets tricky
-- because users may, possibly accidentally, try to apply essences and/or repair
-- already-tinctured tools.  There doesn't appear any obvious way to block the
-- crafting recipe in this case, but then an accidental application could
-- unexpectedly destroy a tinctured item.  In order to workaround this, check
-- for cases where a tinctured item has its tinctures overridden and give the
-- player (or drop) the corresponding essence; since this is a side-effect,
-- don't apply the side-effect when this function is called during craft
-- prediction.
-- FIXME: It feels like there ought to be a better way to detect which recipe
-- triggered a craft.  Either way, the logic should be cleaned up to only
-- traverse the craft grid once.
function tinc_callback_on_craft(itemstack, player, old_craft_grid, craft_inv, predict)
	-- Create dropped essence handler.
	local apply_handler = nil
	if not predict then
		-- Gives the essence to the player.
		apply_handler = function(essence)
			local inventory = player:get_inventory()
			if inventory:room_for_item("main", essence) then
				inventory:add_item("main", essence)
			else
				minetest.add_item(player:get_pos(), essence)
			end
		end
	end

	-- Handle essence recipe.
	local essence = nil
	local tool = nil
	for _, value in pairs(old_craft_grid) do
		-- Get the ingredients.
		if string.match(value:get_name(), "tinctures:essence_%a+") then
			essence = value
		elseif minetest.registered_tools[value:get_name()] then
			tool = value
		end
	end
	if essence and tool then
		if essence:get_name() == tinc_itemstack_essence_name(tool:get_name()) then
			-- Apply essence to original tool.
			tinc_itemstack_essence_apply(tool, essence, apply_handler)
			return tool
		end
	end

	-- Handle tincture recipe.
	local tinctures = {}
	local tools = {}
	for _, value in pairs(old_craft_grid) do
		-- Search for ingredients
		if minetest.get_item_group(value:get_name(), "tincture") ~= 0 then
			tinctures[#tinctures + 1] = value
		elseif minetest.registered_tools[value:get_name()] then
			tools[#tools + 1] = value
		end
	end
	if #tinctures == 1 and #tools == 1 then
		return tinc_apply(tinctures[1], tools[1], player, predict)
	end

	-- Handle repair recipe.
	-- If the result of a craft is a tool, the result matches its
	-- ingredients, and the ingredients are two in number then it's a repair
	-- recipe.
	local ingredients = {}
	local name = itemstack:get_name()
	if not minetest.registered_tools[name] then
		-- Not a repair recipe.
		return
	end
	for _, value in pairs(old_craft_grid) do
		if value:get_name() == name then
			-- Result matches ingredient; track it.
			ingredients[#ingredients + 1] = value
		elseif value:get_name() ~= "" then
			-- Non-result item found; not a repair recipe.
			return
		end
	end
	if #ingredients ~= 2 then
		-- Ingredients aren't two resultants.
		return
	end
	return tinc_callback_on_craft_repair(itemstack, ingredients, apply_handler)
end

-- Allows users to repair tinctured items.  If both items are tinctured then
-- the "first" item's tincture is applied to the resultant and the "second"
-- item's essence is given to the player.
function tinc_callback_on_craft_repair(itemstack, ingredients, apply_handler)
	-- Get tincture levels.
	local a = tinc_itemstack_level_count(ingredients[1])
	local b = tinc_itemstack_level_count(ingredients[2])

	-- Handle none tinctured.
	if a == 0 and b == 0 then
		return nil
	end

	-- Handle both tinctured.
	local ingredient = nil
	if a > 0 and b > 0 then
		-- Drop essence from second item.
		if apply_handler then
			local essence = tinc_itemstack_essence_get(ingredients[2])
			apply_handler(essence)
		end

		-- Apply essence from first item.
		local essence = tinc_itemstack_essence_get(ingredients[1])
		tinc_itemstack_essence_apply(itemstack, essence, nil)
		return itemstack
	end

	-- Handle one tinctured.
	if a > 0 then
		ingredient = ingredients[1]
	elseif b > 0 then
		ingredient = ingredients[2]
	end
	local essence = tinc_itemstack_essence_get(ingredient)
	tinc_itemstack_essence_apply(itemstack, essence, nil)
	return itemstack
end

-- Returns a user-readable string representation of the specified ItemStack's
-- tool capabilities.
function tinc_capabilities_str(stack)
	local capabilities = stack:get_tool_capabilities()
	local message = ""
	for groupcap, values in pairs(capabilities.groupcaps) do
		message = message .. "Groupcap '" .. groupcap .. "' :"
		message = message .. " uses:" .. values.uses
		for give, time in pairs(values.times) do
			message = message .. " " .. tostring(give) .. ": " .. tostring(time)
		end
		message = message .. "\n"
	end
	for groupname, value in pairs(capabilities.damage_groups) do
		message = message .. "Damage group '" .. groupname .. "' : " .. value .. "\n"
	end
	return message
end

-- Convenience function to capitalize a the first character of a string.
function tinc_capitalize(word)
	return string.upper(string.sub(word, 1, 1)) .. string.sub(word, 2, #word)
end

-- Updates the description of the specified ItemStack to reflect applied
-- tincture effects.
function tinc_description_update(itemstack)
	local meta = itemstack:get_meta()
	local description = meta:get_string("tinctures:description")
	for _, tincture in pairs(tinctures) do
		if meta:get_int("tinctures:" .. tincture.effect) > 0 then
			description = description .. "\n" .. minetest.colorize("#00FFFF", tinc_capitalize(tincture.effect) .. " " .. tostring(tinc_roman_numerals(meta:get_int("tinctures:" .. tincture.effect))))
		end
	end
	meta:set_string("description", description)
end

-- Convenience function which returns the total number of all tincture levels
-- applied to the specified item.
function tinc_itemstack_level_count(itemstack)
	local levels = 0
	local meta = itemstack:get_meta()
	for _, tincture in pairs(tinctures) do
		levels = levels + meta:get_int("tinctures:" .. tincture.effect)
	end
	return levels
end

-- Applies the specified essence to the specified ItemStack.  If the specified
-- ItemStack is already tinctured then its essence will be extracted and passed
-- to 'handler' if it's been specified; the intended use is for the handler to
-- place the item in the player's inventory, drop it, &c.
function tinc_itemstack_essence_apply(itemstack, essence, handler)
	-- Handle existing tinctures.
	local essence_old = tinc_itemstack_essence_get(itemstack)
	if essence_old and handler then
		handler(essence_old)
	end

	-- Initialize tool's tinc metadata.
	tinc_metadata_init(itemstack)

	-- Apply essence to tool.
	local meta = itemstack:get_meta()
	local essence_meta = essence:get_meta()
	for _, tincture in pairs(tinctures) do
		local effect = "tinctures:" .. tincture.effect
		meta:set_int(effect, essence_meta:get_int(effect))
		tincture.use(itemstack)
	end

	-- Update tool description.
	tinc_description_update(itemstack)
end

-- Returns the essence item's name for the specified name.
function tinc_itemstack_essence_name(name)
	-- As much as I prefer to use exact names rather than transform them,
	-- Minetest throws errors if an item name has more than one colon in it,
	-- so transform colons into underscores.
	return "tinctures:essence_" .. string.gsub(name, ":", "_")
end

-- Returns an essence for the specified ItemStack; returns nil if the ItemStack
-- is untinctured.
function tinc_itemstack_essence_get(itemstack)
	-- Check for tinctures.
	if itemstack:get_meta():get_int("tinctures") == 0 then
		-- No tinctures on item.
		return nil
	end

	-- Create essence.
	local essence = ItemStack(tinc_itemstack_essence_name(itemstack:get_name()))

	-- Apply tinctures to essence.
	local itemstack_meta = itemstack:get_meta()
	local essence_meta = essence:get_meta()
	for _, tincture in pairs(tinctures) do
		local effect = "tinctures:" .. tincture.effect
		essence_meta:set_int(effect, itemstack_meta:get_int(effect))
	end

	-- Update description.
	essence_meta:set_string("tinctures:description", essence:get_description())
	tinc_description_update(essence)
	essence_meta:set_string("tinctures:description", itemstack_meta:get_string("tinctures:description"))

	-- Return essence.
	return essence
end

-- Initializes tincture metadata on the specified ItemStack.
function tinc_metadata_init(itemstack)
	-- The default capabilities need to be saved for future
	-- applications of the tinctures.  The number of tinctures is
	-- also saved for the same purpose.  "Give" is coined as a name
	-- for the index of the harvest susceptibility (the index value
	-- for "crumbly", "cracky", &c., in contrast to the time value).
	--   tinctures
	--     Whether or not tincture metadata has been initialized.
	--   tinctures:groupcaps:${GROUPCAP}:times:${GIVE}
	--     Default time for the specified groupcap and give.
	--   tinctures:damage_groups:${GROUP}
	--     Default damage done for the specified damage group.
	--   tinctures:description
	--     Default description of the item.
	--   tinctures:durability
	--     Total number of durability (brown) tinctures applied to the item.
	--   tinctures:efficiency
	--     Total number of efficiency (green) tinctures applied to the item.
	--   tinctures:power
	--     Total number of power (red) tinctures applied to the item.
	--   tinctures:punch_attack_uses
	--     Amount of uses tool has for attacking.
	local capabilities = itemstack:get_tool_capabilities()
	local meta = itemstack:get_meta()
	if meta:get_int("tinctures") == 0 then
		meta:set_int("tinctures", 1)
		for groupcap, values in pairs(capabilities.groupcaps) do
			meta:set_int("tinctures:groupcaps:" .. groupcap .. ":uses", values.uses)
			for give, time in pairs(values.times) do
				meta:set_float("tinctures:groupcaps:" .. groupcap .. ":times:" .. tostring(give), values.times[give])
			end
		end
		for groupname, value in pairs(capabilities.damage_groups) do
			meta:set_int("tinctures:damage_groups:" .. groupname, value)
		end
		meta:set_string("tinctures:description", itemstack:get_description())
		for _, tincture in pairs(tinctures) do
			meta:set_int("tinctures:" .. tincture.effect, 0)
		end
		meta:set_int("tinctures:punch_attack_uses", capabilities.punch_attack_uses)
	end
end

-- Returns a string of the specified number as a roman numeral.
function tinc_roman_numerals(number)
	local output = ""
	local wholes = {
		{numeral="M", value=1000},
		{numeral="C", value=100},
		{numeral="X", value=10},
		{numeral="I", value=1}
	}
	local halves = {
		{numeral="D", value=500},
		{numeral="L", value=50},
		{numeral="V", value=5}
	}
	local i = 1
	local j = 1
	while i <= #wholes do
		while wholes[i].value <= number do
			-- Sequential digits.
			output = output .. wholes[i].numeral
			number = number - wholes[i].value
		end
		if i + 1 <= #wholes and wholes[i].value - wholes[i + 1].value <= number then
			-- 9s
			output = output .. wholes[i + 1].numeral .. wholes[i].numeral
			number = number - (wholes[i].value - wholes[i + 1].value)
		end
		if j <= #halves and halves[j].value <= number then
			-- 5s
			output = output .. halves[j].numeral
			number = number - halves[j].value
		end
		if j <= #halves and i + 1 <= #wholes and halves[j].value - wholes[i + 1].value <= number then
			-- 4s
			output = output .. wholes[i + 1].numeral .. halves[j].numeral
			number = number - (halves[j].value - wholes[i + 1].value)
		end
		j = j + 1
		i = i + 1
	end
	return output
end

-- Applies the given tincture to the user's item.
function tinc_apply(tincture, tool, user, predict)
	-- Check for explosion.
	local definition = tincture:get_definition()
	if not predict and math.random(2^definition._brightness) ~= 1 then
		minetest.log("Player '" .. user:get_player_name() .. "' destroys '" .. tool:get_name() .. "' using '" .. tincture:get_name() .. "'")

		-- Knock player back.  Player is knocked backwards +- 15 degrees
		-- horizontally and upwards 10-15 degrees.  Force increases by
		-- 10% per tincture.
		-- Get horizontal direction.
		local magnitude = tinc_itemstack_level_count(tool) + definition._brightness
		local yaw = user:get_look_horizontal()
		yaw = yaw + math.pi + ((math.random() * 2) - 1) * 0.2618
		local myvector = minetest.yaw_to_dir(yaw)
		-- Get vertical direction.
		myvector = vector.rotate(myvector, {x = 0.1745 + math.random() * 0.0873, y = 0, z = 0})
		magnitude = 7.0 * (1 + magnitude / 10)
		myvector = vector.multiply(myvector, magnitude)
		user:add_velocity(myvector)

		-- Explode nodes.
		-- Nodes within the radius of the explosion are removed with
		-- 100% certainty while nodes within radius + 1 are removed
		-- probabalistically.  Volume increases by 10% per tincture.
		-- Volume of sphere (v = 4/3 pi r^3, r = (3v / 4pi)^(1/3))
		local center = user:get_pos()
		local volume = 4 * (magnitude / 10)
		local radius = ((3 * volume) / (4 * math.pi))^(1/3)
		local scope = radius + 2
		-- This is a stupid-brute way to remove nodes but it's good
		-- enough.
		for k = math.floor(center.z - scope), math.floor(center.z + scope) do
			for j = math.floor(center.y - scope), math.floor(center.y + scope) do
				for i = math.floor(center.x - scope), math.floor(center.x + scope) do
					local dx = (i - center.x)^2
					local dy = (j - center.y)^2
					local dz = (k - center.z)^2
					local d = (dx + dy + dz)^(1/2) - radius
					if d <= 0 then
						-- Within radius.
						minetest.dig_node({x=i, y=j, z=k})
					elseif d < 1 then
						-- Near radius.
						if math.random() > d then
							minetest.dig_node({x=i, y=j, z=k})
						end
					end
				end
			end
		end

		-- Spawn shrapnel.
		minetest.add_particlespawner({
			amount = 64,
			time = 0.1,
			minpos = vector.subtract(center, 0.3),
			maxpos = vector.add(center, 0.3),
			minvel = {x = -5, y = -5, z = -5},
			maxvel = {x = 5, y = 5, z = 5},
			minacc = {x = 0, y = 0, z = 0},
			maxacc = {x = 0, y = 0, z = 0},
			minexptime = 1,
			maxexptime = 1,
			minsize = 0.2,
			maxsize = 0.2,
			-- FIXME: A better version of this would use dig
			-- textures from the target item rather than the whole
			-- image, but it appears to be an item and not a node
			-- so I'm not sure how to do that.
			--node = {name = "default:stone"}
			texture = tool:get_definition().inventory_image
		})

		-- Play sound.
		minetest.sound_play("explosion", {
				pos = user:get_pos(),
				gain = 0.5 + (math.min(magnitude * 0.01, 0.5))},
			true)

		-- Player damage.
		local damage = 4 + (magnitude / 5)
		local health = user:get_hp() - damage
		user:set_hp(health)

		-- Remove tincture.
		-- Destroy the tool by giving it max wear.
		tool:set_wear(65536)
		return tool
	end

	-- Save metadata and default capabilities.
	tinc_metadata_init(tool)

	-- Update tool capabilities and metadata.
	local tincture_def = definition._tincture
	local meta = tool:get_meta()
	local meta_name = "tinctures:" .. tincture_def.effect
	local tincture_count = meta:get_int(meta_name) + definition._brightness
	meta:set_int(meta_name, tincture_count)
	tincture_def.use(tool)

	-- Update tool description.
	tinc_description_update(tool)

	-- Give updated tool to player.
	return tool
end

-- Increase durability by 100% per tincture.
function tinc_use_durability(target)
	local meta = target:get_meta()
	local durability = meta:get_int("tinctures:durability")
	local capabilities = target:get_tool_capabilities()
	for _, table in pairs(meta:to_table()) do
		for key, value in pairs(table) do
			local groupcap = string.match(tostring(key), "tinctures:groupcaps:(%w+):uses")
			if groupcap ~= nil then
				capabilities.groupcaps[groupcap].uses = meta:get_int("tinctures:groupcaps:" .. groupcap .. ":uses") * (durability + 1)
			end
		end
	end
	capabilities.punch_attack_uses = meta:get_int("tinctures:punch_attack_uses") * (durability + 1)
	meta:set_tool_capabilities(capabilities)
end

-- Handled by 'register_on_dignode(tinc_callback_efficiency)'
function tinc_use_efficiency(target)
	return
end

-- Increase speed and damage by 10% per tincture.
-- FIXME: Damage is in integers, so fractional damage is discarded by flooring
-- until enough tinctures are applied.  Using 'register_on_punchnode' to apply
-- a "carry" on occasional strikes does not work because mods override
-- 'on_punch' and do not call the registered callbacks.
function tinc_use_power(target)
	local meta = target:get_meta()
	local power = meta:get_int("tinctures:power")
	local capabilities = target:get_tool_capabilities()
	for _, table in pairs(meta:to_table()) do
		for key, value in pairs(table) do
			-- Increase speed.
			local groupcap, give = string.match(tostring(key), "tinctures:groupcaps:(%w+):times:(%d+)")
			if groupcap ~= nil then
				capabilities.groupcaps[groupcap].times[tonumber(give)] = meta:get_float("tinctures:groupcaps:" .. groupcap .. ":times:" .. tonumber(give)) / (1 + (power / 10))
			end

			-- Increase damage.
			local groupname = string.match(tostring(key), "tinctures:damage_groups:(%w+)")
			if groupname ~= nil then
				capabilities.damage_groups[groupname] = meta:get_int("tinctures:damage_groups:" .. groupname) * (1 + power * 0.10)
			end
		end
	end
	meta:set_tool_capabilities(capabilities)
end

-- Register items
tinctures = {
	{color="red", color_css="red", effect="power", use=tinc_use_power},
	{color="brown", color_css="saddlebrown", effect="durability", use=tinc_use_durability},
	{color="green", color_css="green", effect="efficiency", use=tinc_use_efficiency}
}
brightnesses = {"dull", "bright", "brilliant"}
for _, tincture in pairs(tinctures) do
	for i, brightness in pairs(brightnesses) do
		minetest.register_craftitem(":tinctures:" .. tincture.color .. "_" .. brightness, {
			description = tinc_capitalize(brightness) .. " " .. tinc_capitalize(tincture.color) .. " Tincture",
			groups = {tincture = 1},
			inventory_image = "vial.png^(fluid.png^[multiply:" .. tincture.color_css .. ")^(glow_" .. brightness .. ".png^[multiply:" .. tincture.color_css .. ")",
			_brightness = i,
			_tincture = tincture
		})
	end
end

-- Register commands
for _, tincture in pairs(tinctures) do
	minetest.register_chatcommand("tinc_set_" .. tincture.effect, {
		params = "<level>",
		description = "Set tincture effect '" .. tincture.effect .. "' (" .. tincture.color .. ") to the specified level",
		privs = {tincerer=true},
		func = function(name, param)
			-- Validate parameter & tool.
			local valid = true
			param = tonumber(param)
			if param == nil then
				-- Non-numeric level.
				valid = false
			else
				param = math.floor(param)
				if param < 0 then
					-- Negative level.
					valid = false
				end
			end
			if not valid then
				-- Invalid level.
				minetest.chat_send_player(name, "'level' must be a non-negative integer")
			end
			local inventory = minetest.get_inventory({type="player", name=name})
			local target = inventory:get_stack("main", 1)
			if not minetest.registered_tools[target:get_name()] then
				-- Invalid tool.
				minetest.chat_send_player(name, "'" .. target:get_name() .. "' does not appear to be a tool, skipping!")
				valid = false
			end
			if not valid then
				-- Invalid argument(s).
				return true
			end

			-- Save metadata and default capabilities.
			tinc_metadata_init(target)

			-- Update tool capabilities and metadata.
			local meta = target:get_meta()
			local meta_name = "tinctures:" .. tincture.effect
			meta:set_int(meta_name, param)
			tincture.use(target)

			-- Update tool description.
			tinc_description_update(target)

			-- Give updated tool to player.
			inventory:set_stack("main", 1, target)
			return true
		end
	})
end
minetest.register_chatcommand("tinc_caps", {
	params = nil,
	description = "Get capabilities for the currently-wielded item",
	privs = nil,
	func = function(name, param)
		local player = minetest.get_player_by_name(name)
		local stack = player.get_wielded_item(player)
		minetest.chat_send_player(name, tinc_capabilities_str(stack))
		return true
	end
})
minetest.register_chatcommand("tinc_efficiency", {
	params = nil,
	description = "Get stored player efficiencies (an extra ore is generated at 100)",
	privs = nil,
	func = function(name, param)
		local player = minetest.get_player_by_name(name)
		local meta = player:get_meta()
		for key, value in pairs(meta:to_table().fields) do
			-- Print each stored efficiency.
			local node = string.match(key, "tinctures:efficiency:(.+)")
			if node then
				-- Print the efficiency.
				minetest.chat_send_player(name, node .. ": " .. value)
			end
		end
		return true
	end
})

-- Register callbacks
minetest.register_on_dignode(tinc_callback_efficiency)
minetest.register_on_craft(function(itemstack, player, old_craft_grid, craft_inv)
	return tinc_callback_on_craft(itemstack, player, old_craft_grid, craft_inv, false)
end)
minetest.register_craft_predict(function(itemstack, player, old_craft_grid, craft_inv)
	return tinc_callback_on_craft(itemstack, player, old_craft_grid, craft_inv, true)
end)

-- Register privilege
minetest.register_privilege("tincerer", {
	description = "Can set tincture level of items",
	give_to_singleplayer = false
})

-- Override tool breakage
-- This is a bit hacky since it requires loading any tool-defining modules
-- before it can run, but it seems to work so far...
for key, tool in pairs(minetest.registered_tools) do
	-- Create tincture essence item.
	minetest.register_craftitem(tinc_itemstack_essence_name(key), {
		description = "Tincture Essence: " .. tool.description,
		-- The essence image is a shrunken, "ephemeral" (brightened)
		-- version of the tool with a "glow" around it.
		inventory_image = "essence.png" .. "^[combine:32x32:8,8=" .. tool.inventory_image .. "^[brighten",
		stack_max = 1
		}
	)

	-- Override tool breakage.
	after_use = function(itemstack, user, node, digparams)
		-- Extract essence.
		-- This has to be done before wear is added, because after the
		-- tool is broken the metadata won't be set.
		local essence = tinc_itemstack_essence_get(itemstack)

		-- Drop essence on tool breakage.
		local definition = minetest.registered_tools[itemstack:get_name()]
		itemstack:add_wear(digparams.wear)
		if itemstack:get_wear() > 0 or digparams.wear == 0 then
			-- Tool not broken.
			return itemstack
		end
		if not essence then
			-- No tinctures on item; break normally.
			return itemstack
		end
		if definition.sound.breaks then
			-- Returning a new item overrides the breaking sound,
			-- so play it manually.
			minetest.sound_play(definition.sound.breaks)
		end
		return essence
	end
	minetest.override_item(key, {after_use = after_use})

	-- Create essence recipe.
	minetest.register_craft({
		type = "shapeless",
		output = key,
		recipe = {
			tinc_itemstack_essence_name(key),
			key
		}
	})

	-- Create tincture recipe.
	minetest.register_craft({
		type = "shapeless",
		output = key,
		recipe = {
			"group:tincture",
			key
		}
	})
end
