Empower your weapons!
Harden your tools!
Trigger explosions!

![Inventory with tinctures and tinctured diamond pickaxe](screenshot.png)

The `tinctures` mod allows you to permanently enhance tools, but at significant risk to your health and the surrounding area.
The lucky will find themselves wielding new power, while the unlucky will find their limbs lost.
Those brave (or foolish) enough to be lured by the promise of tinctures should read on.

# How to apply tinctures
In order to apply a tincture to a tool, place the tool and the tincture within the crafting grid.
The output prediction will display what effects will be applied to the tool when the tincture is applied, but beware: tinctures are volatile and may instead destroy the both the item, player, and surrounding area.
The more powerful the tincture, the greater the volatility, and the more tinctured the tool, the greater the destruction.

# Essences
An "essence" contains the stable application of a tincture applied to a specific type of tool.
When a tinctured tool breaks, it will drop an essence for that specific type of tool containing the broken tool's tinctures.
The essence and another instance of the same type of tool can then be crafted together in order to produce a tinctured version of the tool, as if the essence's tinctures had been successfully applied to the new instance of the tool.
You might think that essences are a hack workaround for the fact that tools cannot be at zero wear, and you'd be right.
An essence will also be created if the tool repair crafting recipe would destroy a tinctured item.

# List tinctures and effects
Each tincture has a specific color and a corresponding effect.
Tinctures are listed by "Color: Effect".

## Brown: Durability
Increases the durability of the tool.

## Green: Efficiency
Causes extra ore to be mined.
Extra ore that is a fraction of an ore is stored in the tool until a full ore is obtained, at which point a sound is played and the player gets the extra ore.

## Red: Power
Increases digging speed and damage done.

# Known issues
## Damage rounded down to integer increments
Weapon damage is an integer value and fractional parts get truncated.
This means that the Power effect won't actually increase the damage of a weapon until a whole integer worth of damage can be added to the weapon.

## Durability might not properly apply to weapons
Weapon wear is determined by the result of the `object:punch` method and so is the responsibility of the mod implementing the object, not the tool, to determine.
Players will not see any durability benefit when attacking objects added from mods that do not respect tool capabilities such as `punch_attack_uses`.

# Admin notes
The below items are for server administrators.

## Tools must be loaded before tinctures
All tools which can have tinctures applied to them must be registered before the `tinctures` mod is loaded.
This is because at module initialization time the `tinctures` mod will iterate through the list of registered tools and create the requisite crafting recipes.
The known way to ensure another mod is loaded before the `tinctures` mod is to declare the other mod as a dependency of the `tinctures` mod.

## Source of drops must be added to existing mods
This mod does add any source of tinctures to the game world.
Use this mod in conjunction with another mod to distribute tinctures to players.

# License
This work is licensed under the GNU AGPLv3.
See `COPYING` for details.

# Attributions
* sounds/efficiency.ogg: \
	by Killersmurf96 (Original: Gold_Pick.wav) \
	CC BY 4.0 \
	Truncated and converted to OGG \
	https://freesound.org/s/423123
* sounds/explosion.ogg: \
	by sangnamsa \
	CC0 1.0 \
	https://freesound.org/s/473941
